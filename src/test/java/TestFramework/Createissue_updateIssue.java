package TestFramework;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ApiResourse.resourcess;
import ConfigBeforeTest.TestConfig;
import DataResourse.ReusableMethods;
import DataResourse.payLoad;
import static org.hamcrest.Matchers.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Createissue_updateIssue {
	Properties prop;
	String issueid;
	Object baseuri;
	
	@BeforeTest
	public void getData() throws IOException
	{
		
		FileInputStream fis=new FileInputStream(System.getProperty("user.dir")+"/RESTAssuredTest/env.properties");
		prop.load(fis);
		baseuri=prop.get("HOST");
	}
	
	
		
	@Test(priority=1)
	public void createissue() {
		RestAssured.baseURI=(String) baseuri;
		Response res=given().header("x-apikey",TestConfig.getSessionKEY())
					 .body(payLoad.Createissues()).
		              when().post(resourcess.createissueresourse()).then() 
		              .assertThat().log().all().contentType("Json").
		              statusCode(200).and().body("", equalTo("")).extract()
		              .response();
		
		issueid=ReusableMethods.rawToJson(res).get("ID");
	
	}
	@Test(priority=2)
	public void modiifiesissue() {
		RestAssured.baseURI=(String) baseuri;
		Response res=given().header("x-apikey",TestConfig.getSessionKEY())
					 .body(payLoad.Createissues()).
					  when().put(resourcess.Modifyissueresourse()+issueid).then().
					  assertThat().log().all().contentType("Json").statusCode(200).
					  and().body("", equalTo("")).extract()
					.response();
	}
}
