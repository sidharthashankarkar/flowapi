package ConfigBeforeTest;

import static io.restassured.RestAssured.given;

import org.testng.annotations.BeforeClass;

import ApiResourse.resourcess;
import DataResourse.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class TestConfig {

	@BeforeClass
	public static String getSessionKEY()
	{
		RestAssured.baseURI= "http://localhost:8080";
		Response res=given().header("Content-Type", "application/json").
		body("{ \"username\": \"USERNAME\", \"password\": \"PASSWORD\" }").
		when().
		post(resourcess.createissueresourse()).then().statusCode(200).
		extract().response();
		
		JsonPath js= ReusableMethods.rawToJson(res);
		String sessionid= js.get("session.value");
		return sessionid;
	}
}
